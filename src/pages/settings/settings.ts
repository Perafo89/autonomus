import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CONFIG } from '../../providers/config'
import { StorageDb } from '../../providers/storageDb';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  todo: any = {};
  
  constructor(
    public navCtrl: NavController, 
    private db: StorageDb) {

  }

  ionViewDidLoad(){
   this.getServerData();
  }

  ionViewWillLeave(){
   this.setServerData();
  }

  getServerData(){
    this.db.getItem(CONFIG.SERVER_KEY).then(res =>{      
        console.log(JSON.stringify(res))
        if(res != null){
          this.todo = res;      
        }        
    })
  }

  setServerData(){      
      console.log(JSON.stringify(this.todo))
      this.db.setItem(CONFIG.SERVER_KEY, this.todo)
  }

}