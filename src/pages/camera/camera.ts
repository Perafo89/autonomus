import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media';


@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html'
})
export class CameraPage {
  

  constructor(public navCtrl: NavController,
              private streamingMedia: StreamingMedia
            ) {

              let options: StreamingVideoOptions = {
                successCallback: () => { console.log('AUTONOMUS Video played') },
                errorCallback: (e) => { console.log('AUTONOMUS Error streaming ' + e) },
                orientation: 'landscape',
                shouldAutoClose: true,
                controls: true
              };

              //const urlVideo = 'rtsp://admin:perafo89@192.168.0.106:80:554/cam/realmonitor?channel=1&subtype=0&unicast=true&proto=Onvif'
              const urlVideo = 'rtsp://192.168.0.106:80:554/cam/realmonitor?channel=1&subtype=0&unicast=true&proto=Onvif'

              this.streamingMedia.playVideo(urlVideo, options);

    
  }

  
}
