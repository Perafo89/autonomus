import { Injectable} from '@angular/core'
import {Platform} from 'ionic-angular'
import { StorageDb } from './storageDb';
import { CONFIG } from './config';
import { Http } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import 'rxjs/add/operator/map';
import { isTrueProperty } from 'ionic-angular/umd/util/util';



@Injectable()

    export class HubApi{        
        
        constructor(
          public db: StorageDb, 
          private http: HTTP, 
          public plt: Platform){
            
        }        

        get(path){
          if(this.getPlatformType()){
            this.getRelativePath(path).then(res =>{
              console.log(JSON.stringify(res))
              const urlApi = res['url'];
              console.log('urlApi ' + urlApi)
              this.http.get(urlApi, {}, {})
              .then(data => {
  
                console.log(data.status);
                console.log(data.data); // data received by server
                console.log(data.headers);
            
              })
              .catch(error => {
            
                console.log(error.status);
                console.log(error.error); // error message as string
                console.log(error.headers);
            
              });
            })
          }
        }

        post(url, body, headers){
          console.log(`url ${url } body ${body} headers ${headers}`)
          if(this.getPlatformType()){
            return new Promise((resolve, reject) =>{      
                this.http.setDataSerializer('utf8');        
                this.http.post(url, body, headers)
                .then(res =>{
                  console.log(`HubApi post result  ${res}`)
                  resolve(res)
                })
                .catch(e =>{
                  console.error(`HubApi post ERROR :(  ${e}`)
                })            
            })        
          }
        }

        getPlatformType(){
          if(this.plt.is('ios') || this.plt.is('android')){
            return true
          }
          return false
        }

        getRelativePath(path){
          return new Promise((resolve, reject) =>{
            this.db.getItem(CONFIG.SERVER_KEY).then(res =>{
              resolve({
                url: res.url + path
              })
            }).catch(reject)
          })
        }

        
    }