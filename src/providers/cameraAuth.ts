import { Injectable} from '@angular/core'
import { HttpInterceptor, HttpRequest, HttpHandler} from '@angular/common/http'
@Injectable()
export class CameraAuth implements HttpInterceptor {
  constructor( ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    let auth: any = '30c8e437594ee0095848624bdb870e32';    
    const newReq = req.clone({ headers: req.headers.set('Authorization', 'Basic ' + auth) });
    return next.handle(newReq);
  }
};
